//
//  Constants.swift
//  Pokedex3
//
//  Created by Yaroslav Zhurbilo on 21.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation

typealias DownloadComplete = () -> ()
