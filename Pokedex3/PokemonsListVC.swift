//
//  PokemonsListVC.swift
//  Pokedex3
//
//  Created by Yaroslav Zhurbilo on 19.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import AVFoundation

class PokemonsListVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var pokemons = [Pokemon]()
    var pokemonsDB = [Pokemon]()
    var audioPlayer: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        searchBar.delegate = self
        
        searchBar.returnKeyType = UIReturnKeyType.done
        
        parsePokemonCSV()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            pokemons = pokemonsDB
            view.endEditing(true)
        } else {
            pokemons.removeAll()
            for pokemon in pokemonsDB {
                if pokemon.name.lowercased().contains(searchText.lowercased()) {
                    pokemons.append(pokemon)
                }
            }
        }
        collectionView.reloadData()
        
    }
    
    func parsePokemonCSV() {
        if let path = Bundle.main.path(forResource: "pokemon", ofType: "csv") {
            do {
                let csv = try CSV(contentsOfURL: path)
                let rows = csv.rows
                for row in rows {
                    guard let name = row["identifier"] else { return }
                    guard let rowID = row["id"], let id = Int(rowID) else { return }
                    let newPokemon = Pokemon(name: name, pokedexId: id)
                    pokemons.append(newPokemon)
                }
                pokemonsDB = pokemons
                
            } catch {
                print(error.localizedDescription)
            }
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokeCell", for: indexPath) as? PokeCell {
            
            cell.configureCell(pokemon: pokemons[indexPath.row])
            return cell
            
        } else {
            return UICollectionViewCell()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PokemonDetailsShow", sender: pokemons[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? PokemonDetailsVC else { return }
        guard let pokemon = sender as? Pokemon else { return }
        destination.pokemon = pokemon
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 100, height: 100)
    }
    
    @IBAction func playMusic(_ sender: UIButton) {
        if audioPlayer != nil {
            audioPlayer.stop()
            audioPlayer = nil
            return
        }
        guard let audioURL = Bundle.main.url(forResource: "music", withExtension: "mp3") else { return }
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: audioURL)
            audioPlayer.play()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func unwindToPokemonsListVC(segue: UIStoryboardSegue) {
        
    }
    
}






