//
//  Pokemon.swift
//  Pokedex3
//
//  Created by Yaroslav Zhurbilo on 20.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation
import Alamofire

class Pokemon {
    private var _name: String!
    private var _description: String!
    private var _type: String!
    private var _defense: String!
    private var _height: String!
    private var _pokedexId: Int!
    private var _weight: String!
    private var _baseAttack: String!
    private var _nextEvolutionName: String!
    private var _nextEvolutionPokedexID: String!
    private var _pokemonURL: String!
    
    // --- Getters --- //
    
    var name: String {
        if _name == nil {
            _name = ""
        }
        return _name
    }
    var description: String {
        if _description == nil {
            _description = ""
        }
        return _description
    }
    var type: String {
        if _type == nil {
            _type = ""
        }
        return _type
    }
    var defense: String {
        if _defense == nil {
            _defense = ""
        }
        return _defense
    }
    var height: String {
        if _height == nil {
            _height = ""
        }
        return _height
    }
    var pokedexId: Int {
        if _pokedexId == nil {
            _pokedexId = 0
        }
        return _pokedexId
    }
    var weight: String {
        if _weight == nil {
            _weight = ""
        }
        return _weight
    }
    var baseAttack: String {
        if _baseAttack == nil {
            _baseAttack = ""
        }
        return _baseAttack
    }
    var nextEvolutionName: String {
        if _nextEvolutionName == nil {
            _nextEvolutionName = ""
        }
        return _nextEvolutionName
    }
    var nextEvolutionPokedexID: String {
        if _nextEvolutionPokedexID == nil {
            _nextEvolutionPokedexID = ""
        }
        return _nextEvolutionPokedexID
    }
    
    // --- Initializers --- //
    
    init(name: String, pokedexId: Int) {
        _name = name
        _description = ""
        _type = ""
        _defense = ""
        _height = ""
        _pokedexId = pokedexId
        _weight = ""
        _baseAttack = ""
        _nextEvolutionName = ""
        _nextEvolutionPokedexID = ""
        _pokemonURL = PokemonAPI(pokedexID: pokedexId).urlToRequest
    }
    
    func downloadPokemonDetails(completed: @escaping DownloadComplete) {
        guard let url = URL(string: self._pokemonURL) else { return }
        Alamofire.request(url).responseJSON { response in
            guard let value = response.result.value as? Dictionary<String, Any> else { return }
            
            if let descriptionURI = (value["descriptions"] as? [Dictionary<String,String>])?[0]["resource_uri"] {
                
                let descURL = PokemonAPI().urlBase + descriptionURI
                guard let urlToGetDescription = URL(string: descURL)
                    else { return }
                
                Alamofire.request(urlToGetDescription).responseJSON(completionHandler: { response in
                    
                    guard let description = (response.value as? Dictionary<String, Any>)?["description"] as? String
                        else { return }
                    self._description = description
                    completed()
                })
            }
            
            if let evolutionsDict = value["evolutions"] as? [Dictionary<String,Any>], evolutionsDict.count > 0 {
                
                guard let nextEvolutionName = evolutionsDict[0]["to"] as? String else { return }
                guard let evoUri = evolutionsDict[0]["resource_uri"] as? String else { return }
                
                let replacedURI = evoUri.replacingOccurrences(of: "/api/v1/pokemon/", with: "")
                let nextEvoPokedexID = replacedURI.replacingOccurrences(of: "/", with: "")
                
                self._nextEvolutionName = nextEvolutionName
                self._nextEvolutionPokedexID = nextEvoPokedexID
            }
            
            if let requestedTypes = value["types"] as? [Dictionary<String,String>], requestedTypes.count > 0 {
                self._type = ""
                for type in requestedTypes {
                    guard let typeName = type["name"] else { return }
                    var delimiter = ""
                    if self.type != "" {
                        delimiter = "/"
                    }
                    self._type = self._type + delimiter + typeName.capitalized
                }
            }
            if let requestedDefense = value["defense"] as? Int {
                self._defense = "\(requestedDefense)"
            }
            if let requestedHeight = value["height"] as? String {
                self._height = "\(requestedHeight)"
            }
            if let requestedWeight = value["weight"] as? String {
                self._weight = "\(requestedWeight)"
            }
            if let requestedAttack = value["attack"] as? Int {
                self._baseAttack = "\(requestedAttack)"
            }
            
            completed()
        }
    }
    
    convenience init() {
        self.init(name: "", pokedexId: 0)
    }
}
