//
//  PokemonDetailsVC.swift
//  Pokedex3
//
//  Created by Yaroslav Zhurbilo on 20.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit

class PokemonDetailsVC: UIViewController {
    
    var pokemon: Pokemon = Pokemon(name: "", pokedexId: 0)
    
    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var defenseLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var pokedexIdLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var baseAttackLabel: UILabel!
    @IBOutlet weak var nextEvolutionLabel: UILabel!
    @IBOutlet weak var currentEvolutionImage: UIImageView!
    @IBOutlet weak var nextEvolutionImage: UIImageView!    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pokemonNameLabel.text = pokemon.name.capitalized
        mainImage.image = UIImage(named: "\(pokemon.pokedexId)")
        pokedexIdLabel.text = "\(pokemon.pokedexId)"
        currentEvolutionImage.image = UIImage(named: "\(pokemon.pokedexId)")
        
        pokemon.downloadPokemonDetails { 
            //TODO: Write here something
            self.updateUI()
        }
        
    }
    
    func updateUI() {
        
        descriptionLabel.text = pokemon.description
        typeLabel.text = pokemon.type
        defenseLabel.text = pokemon.defense
        heightLabel.text = pokemon.height
        pokedexIdLabel.text = "\(pokemon.pokedexId)"
        weightLabel.text = pokemon.weight
        baseAttackLabel.text = pokemon.baseAttack
        
        if pokemon.nextEvolutionPokedexID == ""
            || pokemon.nextEvolutionName.lowercased().contains("mega")
            || pokemon.nextEvolutionPokedexID == "0" {
            
            nextEvolutionImage.isHidden = true
            nextEvolutionLabel.text = "No evolution"
            
        } else {
            
            nextEvolutionImage.isHidden = false
            nextEvolutionLabel.text = "Next Evolution: " + pokemon.nextEvolutionName
            nextEvolutionImage.image = UIImage(named: pokemon.nextEvolutionPokedexID)
            
        }
    }
    
    
}












