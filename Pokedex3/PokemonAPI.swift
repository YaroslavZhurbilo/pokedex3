//
//  PokemonAPI.swift
//  Pokedex3
//
//  Created by Yaroslav Zhurbilo on 21.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation

class PokemonAPI {
    
    // --- Private variables --- //
    
    private var _pokedexID: Int!
    
    private var _requestType: RequestType
    
    private var pokedexID: Int {
        if _pokedexID == nil {
            return 0
        } else {
            return self._pokedexID
        }
    }
    
    private var _baseURL = "http://pokeapi.co"
    
    private var _pokemonURL: String {
        var url = "/api/v1/pokemon/"
        
        switch _requestType {
        case .Pokemon:
            url += "\(pokedexID)"
        }
        
        return url
    }
    
    // --- Public variables --- //
    
    var urlToRequest: String {
        var url = _baseURL
        
        switch _requestType {
        case .Pokemon:
            url += _pokemonURL
        }
        
        return url
    }
    
    var urlBase: String {
        return _baseURL
    }
    
    // --- Initializers --- //
    
    init(pokedexID id: Int, requestType: RequestType = .Pokemon) {
        self._pokedexID = id
        self._requestType = requestType
    }
    
    convenience init() {
        self.init(pokedexID: 0)
    }
    
    // --- Other --- //
    
    enum RequestType {
        case Pokemon
    }
    
}





